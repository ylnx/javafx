package org.csu.linkgame;

import java.net.URL;

import java.util.ResourceBundle;

import javafx.fxml.FXML;

import javafx.fxml.Initializable;

import javafx.scene.web.WebEngine;

import javafx.scene.web.WebView;

public class Controller implements Initializable {
    @FXML
    private WebView webView;

    @Override

    public void initialize(URL location, ResourceBundle resources) {
// TODO Auto-generated method stub

        final WebEngine webengine = webView.getEngine();

        String url = test3.class.getResource("view/test.html").toExternalForm();

        webengine.load(url);

    }

}