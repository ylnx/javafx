package org.csu.linkgame;

import org.csu.linkgame.domain.GameLevel;
import org.csu.linkgame.service.*;
import org.csu.linkgame.utils.ServiceUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class LinkGameApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private UserService userService;
    @Autowired
    private GameLevelService gameLevelService;
    @Autowired
    private LogService logService;
    @Autowired
    private GameHistoryService gameHistoryService;
    @Test
    public void test() {
//        Collection<String> usernameList = new ArrayList<>();
//        usernameList.add("asdasd");
//        usernameList.add("asdasdasd");
//        System.out.println(userMapper.deleteByUsernameIn(usernameList));

        System.out.println(gameHistoryService.getGameHistoriesByKeyword("j2ee"));
    }


    @Autowired
    private OSSService ossService;
    @Test
    public void testOssUpload(){
        // 参数1 localFilePath：本地绝对路径（用户上传文件）; 参数2 keyPath：image/用户上传文件名xxx.jpg/png； 返回url:orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/xxx.jpg/png
        String url = ossService.upload("D:\\coding\\javafx\\src\\main\\resources\\org\\csu\\linkgame\\images\\icon.jpg","images/icon.jpg");
        System.out.println(url);
    }

    @Test
    public void test1() {
        logService.insertLog("j2ee进入了多人联机模式");
    }
}
