package org.csu.linkgame;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;

import javafx.scene.Scene;

import javafx.stage.Stage;

public class test3 extends Application {
    @Override

    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view/sample.fxml"));

        primaryStage.setTitle("JAVAFX嵌入html测试");

        primaryStage.setScene(new Scene(root, 1270, 860));

        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);

    }

}


