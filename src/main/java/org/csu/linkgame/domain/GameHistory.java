package org.csu.linkgame.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName game_history
 */
@TableName(value ="game_history")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameHistory implements Serializable {
    /**
     * 历史记录id
     */
    @TableId(type = IdType.AUTO)
    private Integer gameHistoryId;

    /**
     * 
     */
    private Date endTime;

    /**
     * 
     */
    private String winner;

    /**
     * 
     */
    private String loser;

    /**
     * 
     */
    private String gameLevel;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        GameHistory other = (GameHistory) that;
        return (this.getGameHistoryId() == null ? other.getGameHistoryId() == null : this.getGameHistoryId().equals(other.getGameHistoryId()))
            && (this.getEndTime() == null ? other.getEndTime() == null : this.getEndTime().equals(other.getEndTime()))
            && (this.getWinner() == null ? other.getWinner() == null : this.getWinner().equals(other.getWinner()))
            && (this.getLoser() == null ? other.getLoser() == null : this.getLoser().equals(other.getLoser()))
            && (this.getGameLevel() == null ? other.getGameLevel() == null : this.getGameLevel().equals(other.getGameLevel()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getGameHistoryId() == null) ? 0 : getGameHistoryId().hashCode());
        result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
        result = prime * result + ((getWinner() == null) ? 0 : getWinner().hashCode());
        result = prime * result + ((getLoser() == null) ? 0 : getLoser().hashCode());
        result = prime * result + ((getGameLevel() == null) ? 0 : getGameLevel().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", gameHistoryId=").append(gameHistoryId);
        sb.append(", endTime=").append(endTime);
        sb.append(", winner=").append(winner);
        sb.append(", loser=").append(loser);
        sb.append(", gameLevel=").append(gameLevel);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}