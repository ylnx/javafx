package org.csu.linkgame.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 
 * @TableName user
 */
@TableName(value ="user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    /**
     * 登录账户、用户名
     */
    @TableId
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 是否为管理员
     */
    private Integer isAdmin;

    /**
     * 玩家排名
     */
    private String userRank;

    /**
     * 玩家积分
     */
    private String userGoal;

    /**
     * 玩家端口号
     */
    private String userPort;

    /**
     * 玩家好友列表
     */
    private String userFriend;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        User other = (User) that;
        return (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getNickname() == null ? other.getNickname() == null : this.getNickname().equals(other.getNickname()))
            && (this.getIsAdmin() == null ? other.getIsAdmin() == null : this.getIsAdmin().equals(other.getIsAdmin()))
            && (this.getUserRank() == null ? other.getUserRank() == null : this.getUserRank().equals(other.getUserRank()))
            && (this.getUserGoal() == null ? other.getUserGoal() == null : this.getUserGoal().equals(other.getUserGoal()))
            && (this.getUserPort() == null ? other.getUserPort() == null : this.getUserPort().equals(other.getUserPort()))
            && (this.getUserFriend() == null ? other.getUserFriend() == null : this.getUserFriend().equals(other.getUserFriend()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getNickname() == null) ? 0 : getNickname().hashCode());
        result = prime * result + ((getIsAdmin() == null) ? 0 : getIsAdmin().hashCode());
        result = prime * result + ((getUserRank() == null) ? 0 : getUserRank().hashCode());
        result = prime * result + ((getUserGoal() == null) ? 0 : getUserGoal().hashCode());
        result = prime * result + ((getUserPort() == null) ? 0 : getUserPort().hashCode());
        result = prime * result + ((getUserFriend() == null) ? 0 : getUserFriend().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", nickname=").append(nickname);
        sb.append(", isAdmin=").append(isAdmin);
        sb.append(", userRank=").append(userRank);
        sb.append(", userGoal=").append(userGoal);
        sb.append(", userPort=").append(userPort);
        sb.append(", userFriend=").append(userFriend);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}