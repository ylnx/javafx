package org.csu.linkgame.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName game_level
 */
@TableName(value ="game_level")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameLevel implements Serializable {
    /**
     * 游戏等级
     */
    @TableId
    private Integer level;

    /**
     * 单元格大小
     */
    private Integer unitSize;

    /**
     * 场景宽度
     */
    private Integer sceneWidth;

    /**
     * 场景长度
     */
    private Integer sceneLength;

    /**
     * 行数
     */
    private Integer rowCount;

    /**
     * 列数
     */
    private Integer colCount;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        GameLevel other = (GameLevel) that;
        return (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
            && (this.getUnitSize() == null ? other.getUnitSize() == null : this.getUnitSize().equals(other.getUnitSize()))
            && (this.getSceneWidth() == null ? other.getSceneWidth() == null : this.getSceneWidth().equals(other.getSceneWidth()))
            && (this.getSceneLength() == null ? other.getSceneLength() == null : this.getSceneLength().equals(other.getSceneLength()))
            && (this.getRowCount() == null ? other.getRowCount() == null : this.getRowCount().equals(other.getRowCount()))
            && (this.getColCount() == null ? other.getColCount() == null : this.getColCount().equals(other.getColCount()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getUnitSize() == null) ? 0 : getUnitSize().hashCode());
        result = prime * result + ((getSceneWidth() == null) ? 0 : getSceneWidth().hashCode());
        result = prime * result + ((getSceneLength() == null) ? 0 : getSceneLength().hashCode());
        result = prime * result + ((getRowCount() == null) ? 0 : getRowCount().hashCode());
        result = prime * result + ((getColCount() == null) ? 0 : getColCount().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", level=").append(level);
        sb.append(", unitSize=").append(unitSize);
        sb.append(", sceneWidth=").append(sceneWidth);
        sb.append(", sceneLength=").append(sceneLength);
        sb.append(", rowCount=").append(rowCount);
        sb.append(", colCount=").append(colCount);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}