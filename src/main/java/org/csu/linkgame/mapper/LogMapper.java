package org.csu.linkgame.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Collection;
import java.util.List;

import org.csu.linkgame.domain.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Pluto
* @description 针对表【log】的数据库操作Mapper
* @createDate 2022-09-02 16:48:17
* @Entity org.csu.linkgame.domain.Log
*/
@Mapper
public interface LogMapper extends BaseMapper<Log> {

    List<Log> selectAll();

    int deleteByLogIdIn(@Param("logIdList") Collection<Integer> logIdList);

    List<Log> selectByKeyword(@Param("keyword") String keyword);

    int insertAll(Log log);
}




