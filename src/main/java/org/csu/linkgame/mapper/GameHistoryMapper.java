package org.csu.linkgame.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Collection;
import java.util.List;

import org.csu.linkgame.domain.GameHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Pluto
* @description 针对表【game_history】的数据库操作Mapper
* @createDate 2022-09-02 16:43:22
* @Entity org.csu.linkgame.domain.GameHistory
*/
@Mapper
public interface GameHistoryMapper extends BaseMapper<GameHistory> {

    List<GameHistory> selectALl();

    int deleteByGameHistoryIdIn(@Param("gameHistoryIdList") Collection<Integer> gameHistoryIdList);

    List<GameHistory> selectByKeyword(@Param("keyword") String keyword);
}




