package org.csu.linkgame.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.csu.linkgame.domain.GameLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Pluto
* @description 针对表【game_level】的数据库操作Mapper
* @createDate 2022-09-02 17:53:12
* @Entity org.csu.linkgame.domain.GameLevel
*/
@Mapper
public interface GameLevelMapper extends BaseMapper<GameLevel> {

    List<GameLevel> selectAll();

    int deleteByLevelIn(@Param("levelList") Collection<Integer> levelList);

    List<GameLevel> selectByKeyword(@Param("keyword") String keyword);

    int insertAll(GameLevel gameLevel);

    int updateSelective(GameLevel gameLevel);

    List<GameLevel> selectAllByLevel(@Param("level") Integer level);
}




