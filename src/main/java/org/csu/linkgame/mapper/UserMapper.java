package org.csu.linkgame.mapper;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import org.csu.linkgame.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author Pluto
* @description 针对表【user】的数据库操作Mapper
* @createDate 2022-08-31 10:51:55
* @Entity org.csu.linkgame.domain.User
*/
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> selectAllByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    int insertSelective(User user);

    List<User> selectAllByUsername(@Param("username") String username);

    List<User> selectALl();

    int deleteByUsernameIn(@Param("usernameList") Collection<String> usernameList);

    List<User> selectByKeyword(@Param("keyword") String keyword);

    int insertAll(User user);

    int updateSelective(User user);
}




