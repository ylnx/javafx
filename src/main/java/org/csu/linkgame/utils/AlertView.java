package org.csu.linkgame.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class AlertView extends Alert {
    public AlertView(AlertType alertType) {
        super(alertType);
    }

    public AlertView(AlertType alertType, String contentText, ButtonType... buttons) {
        super(alertType, contentText, buttons);
    }

    public void showAlert(AlertType alertType, String title,  String contentText, ButtonType... buttons) {
        AlertView alertView = new AlertView(alertType, contentText, buttons);
        alertView.setTitle(title);
        alertView.showAndWait();
    }
}
