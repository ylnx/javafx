package org.csu.linkgame.utils;

import org.csu.linkgame.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;

@Component
public class ServiceUtil implements Serializable {

    @Resource
    public UserService userService;
    @Resource
    public GameLevelService gameLevelService;
    @Resource
    public GameHistoryService gameHistoryService;
    @Resource
    public LogService logService;
    @Resource
    public OSSService ossService;

    public static ServiceUtil serviceUtil;

    @PostConstruct
    public void init() {
        serviceUtil = this;
    }
}
