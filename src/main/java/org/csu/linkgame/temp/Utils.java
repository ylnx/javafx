package org.csu.linkgame.temp;

import javafx.scene.image.Image;

/**
 * @author ylnx
 * 
 */
public interface Utils {
	// 游戏宽度
	public int SCREENWIDTH = 1820;
	// 游戏高度
	public int SCREENHEIGHT = 1000;
	// 控制面板宽度
	public int CONTROLWIDTH = 200;
	// 控制面板高度
	public int CONTROLHEIGHT = SCREENHEIGHT;
	// 画布水平偏移
	public int xOffset = 100;
	// 画布垂直偏移
	public int yOffset = 50;
	// 单元格大小
	public int ELEMENTSIZE = 100;
	// 单元格行数
	public int ROWCOUNT = 12;
	// 单元格列数
	public int COLCOUNT = 12;
	// 单元格图片
	public String[] IMAGE_NAMES = { "1.jpg", "2.jpg",
			"3.jpg", "4.jpg", "5.jpg", "6.jpg",
			"7.jpg", "8.jpg", "9.jpg", "10.jpg" };
	// logo
	//public Image iconImg = new Image("file:D:\\软件工程实验\\实训\\java实训\\src\\main\\java\\org\\csu\\linkgame\\service\\ClassForGame\\images\\bg1.jpg");
	public Image iconImg = new Image("http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/icon.jpg");
	// 背景图片
	String bgImg = "file:C:\\Users\\Lenovo\\Desktop\\image";
	public Image[] BG_IMAGE_NAMES = new Image[] {
			new Image(bgImg+"\\bg1.jpg"),
			new Image(bgImg+"\\bg2.jpg"),
			new Image(bgImg+"\\bg3.jpg"),
			new Image(bgImg+"\\bg4.jpg"),
			new Image(bgImg+"\\bg5.jpg"),
			new Image(bgImg+"\\bg6.jpg"),
	};

}
