package org.csu.linkgame.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 自定义配置application.yml的图片服务器
 */
@ConfigurationProperties(prefix = "image.server")
@Data
@Component
public class ImageServerConfig {
    private String url;
    private String username;
    private String password;
}
