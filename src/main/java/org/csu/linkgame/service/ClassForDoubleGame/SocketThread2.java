package org.csu.linkgame.service.ClassForDoubleGame;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class SocketThread2 implements Runnable{
    public Socket socket;
    public dgServiceImpl canvas;
    public ObjectInputStream is = null;
    public SocketThread2(Socket socket,dgServiceImpl canvas,ObjectInputStream is){
        this.socket = socket;
        this.canvas = canvas;
        this.is = is;
    }
    public SocketThread2(Socket socket,dgServiceImpl canvas){
        this.socket = socket;
        this.canvas = canvas;
    }

    @Override
    public void run() {
        while(true){
            ObjectInputStream is = null;
            try {
                is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                Object obj = is.readObject();
                List<dgPoint> path = (List<dgPoint>)obj;
                System.out.println("这里是Client端,接收到了服务端传来的path:"+path);
                canvas.drawLine(path);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
