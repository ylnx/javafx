package org.csu.linkgame.service.ClassForDoubleGame;

import javafx.stage.Stage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class doubleGameServer extends Thread{
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/super_link?useUnicode=true&characterEncoding=utf8";


    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "rootatcsu";
    public static int userNumber = 0;
    public static List<Socket> socketList = new ArrayList<Socket>();
    public static List<Integer> randomRowList = new ArrayList<>();
    public static List<Integer> randomColList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9999);
        System.out.println("聊天室开启");
        // 初始化数据
        Random random = new Random();
        for (int row = 0; row < 12; row++) {
            for (int col = 0; col < 12; col++) {
                int randomRow = random.nextInt(12);
                int randomCol = random.nextInt(12);
                randomRowList.add(randomRow);
                randomColList.add(randomCol);
            }
        }
        while (true) {
            Socket socket= serverSocket.accept();     //从连接请求队列中取出一个连接

            System.out.println("上线通知： 用户" + socket.getPort()+"上线啦！");
            socketList.add(socket);
            // 初始时发送游戏数据
            ObjectOutputStream os = null;
            os = new ObjectOutputStream(socket.getOutputStream());
            os.writeObject(randomRowList);
            os.flush();
            os.writeObject(randomColList);
            os.flush();
            System.out.println("我已发送列表");

            new Thread(new dgServerThread(socket,randomRowList,randomColList)).start();
        }
    }
}
