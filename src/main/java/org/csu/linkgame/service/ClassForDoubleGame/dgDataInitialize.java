package org.csu.linkgame.service.ClassForDoubleGame;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Random;

public class dgDataInitialize extends Application {
/*    @Autowired
    private GamesettingService gamesettingService;*/
// MySQL 8.0 以上版本 - JDBC 驱动名及数据库 URL
static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/super_link?useUnicode=true&characterEncoding=utf8";


    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "rootatcsu";

    public int level = 0;
    public dgDataInitialize(int level){
        this.level = level;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // 监听窗口关闭
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                //dgClock.tmp = 0;
                //dgClock.flag = 0;

                // 关闭这条时间线
                //clock.stop();
            }
        });
        //clock = new dgClock();

        Group root = new Group();


        Connection conn = null;
        Statement stmt = null;

        // 注册 JDBC 驱动
        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        stmt = conn.createStatement();
        String sql;
        sql = "select * from game_level\n" +
                "where level = "+level;
        ResultSet rs = stmt.executeQuery(sql);

        // 展开结果集数据库
        while(rs.next()){
            // 通过字段检索
            dgUtils.ELEMENTSIZE = rs.getInt("unit_size");
            dgUtils.SCREENWIDTH = rs.getInt("scene_width");
            dgUtils.SCREENHEIGHT = rs.getInt("scene_length");
            dgUtils.ROWCOUNT = rs.getInt("row_count");
            dgUtils.COLCOUNT = rs.getInt("col_count");
        }
        // 完成后关闭
        rs.close();
        stmt.close();
        conn.close();
        // 随机生成背景图片
        Random random = new Random();
        Image image = new Image("file:C:\\Users\\Lenovo\\Desktop\\image\\bg1.jpg");
        ImageView bgView = new ImageView();
        bgView.setImage(image);
        bgView.setFitHeight(dgUtils.SCREENHEIGHT);
        bgView.setFitWidth(dgUtils.SCREENWIDTH);
        root.getChildren().add(bgView);

        dgServiceImpl canvas = new dgServiceImpl();
        // 控制台
        dgButtonsPanel panel = new dgButtonsPanel(canvas, bgView);
        panel.setTranslateX(dgUtils.SCREENWIDTH / 5 );
        panel.setTranslateY(dgUtils.SCREENHEIGHT - dgUtils.CONTROLHEIGHT);
        root.getChildren().addAll(canvas, panel);
        //root.getChildren().add(clock);
        Scene scene = new Scene(root, dgUtils.SCREENWIDTH, dgUtils.SCREENHEIGHT);

        scene.setFill(Color.GRAY);

        primaryStage.setScene(scene);
        primaryStage.setTitle("超级连连看--麻俊鹏组");
        primaryStage.setResizable(false);

        // 添加图标
        Image iconImage = new Image("file:D:\\软件工程实验\\实训\\java实训\\src\\main\\java\\org\\csu\\linkgame\\service\\ClassForGame\\images\\bg1.jpg");

        primaryStage.getIcons().add(dgUtils.iconImg);
        //primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    //}
}
