package org.csu.linkgame.service.ClassForDoubleGame;


import java.io.Serializable;

/**
 * @author ylnx
 *
 */
public class dgPoint implements Serializable {
    // 所在行
    public int row;
    // 所在列
    public int col;
    // 用于查询邻居节点时记录访问次数
    public int count = -1;

    public dgPoint(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        org.csu.linkgame.service.ClassForDoubleGame.dgPoint p = (org.csu.linkgame.service.ClassForDoubleGame.dgPoint) obj;
        return (this.row == p.row) && (this.col == p.col);
    }

    @Override
    public String toString() {
        return "[" + row + "," + col + "," + count + "]";
    }
}
