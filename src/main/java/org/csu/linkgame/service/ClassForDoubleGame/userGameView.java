package org.csu.linkgame.service.ClassForDoubleGame;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


import java.awt.*;
import java.io.BufferedInputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class userGameView extends Application {

// MySQL 8.0 以上版本 - JDBC 驱动名及数据库 URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/super_link?useUnicode=true&characterEncoding=utf8";


    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "rootatcsu";

    public int level = 2;
    // 存储剩余的Uint
    public Map<dgPoint, dgTile> dgMap;
    // 存储Uint的边界bound
    public Map<dgPoint, dgBound> dgBoundMap;

    public int randomRow;
    public int randomCol;
    public AnchorPane gameWindow;
    public userGameView(Map<dgPoint, dgTile> dgMap, Map<dgPoint, dgBound> dgBoundMap,AnchorPane gameWindow){
        this.dgMap = dgMap;
        this.dgBoundMap = dgBoundMap;
        this.gameWindow = gameWindow;
    }

    public userGameView(int randomRow, int randomCol){
        this.randomRow = randomRow;
        this.randomCol = randomCol;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        AnchorPane root = new AnchorPane ();

        Connection conn = null;
        Statement stmt = null;

        // 注册 JDBC 驱动
        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        stmt = conn.createStatement();
        String sql;
        sql = "select * from game_level\n" +
                "where level = "+level;
        ResultSet rs = stmt.executeQuery(sql);

        // 展开结果集数据库
        while(rs.next()){
            // 通过字段检索
            dgUtils.ELEMENTSIZE = rs.getInt("unit_size");
            dgUtils.SCREENWIDTH = rs.getInt("scene_width");
            dgUtils.SCREENHEIGHT = rs.getInt("scene_length");
            dgUtils.ROWCOUNT = rs.getInt("row_count");
            dgUtils.COLCOUNT = rs.getInt("col_count");
        }
        // 完成后关闭
        rs.close();
        conn.close();
        stmt.close();


        // 客户端连接线程,监听服务端的数据的代码在下面，传入canvas的那里
        Socket socket = new Socket("127.0.0.1", 9999);
        System.out.println("恭喜你与服务器连接成功!");
        // 获取服务端的游戏数据
        ObjectInputStream is = null;
        is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        Object obj = is.readObject();
        List<Integer> randomRowList = (List<Integer>)obj;

        Object obj2 = is.readObject();
        List<Integer> randomColList = (List<Integer>)obj2;
        System.out.println("我已接受Row列表:"+randomRowList);
        System.out.println("我已接受Col列表:"+randomColList);

        // 随机生成背景图片
        Random random = new Random();
        Image image = new Image("http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/bg1.jpg");
        ImageView bgView = new ImageView();
        bgView.setImage(image);
        bgView.setFitHeight(dgUtils.SCREENHEIGHT);
        bgView.setFitWidth(dgUtils.SCREENWIDTH);
        gameWindow.getChildren().add(bgView);

        dgServiceImpl canvas = new dgServiceImpl(randomRowList,randomColList,socket);
        // 新开线程监听服务端发来的数据,此处的is是网络输入流
        new Thread(new SocketThread2(socket,canvas)).start();
        // 控制台
        //dgButtonsPanel panel = new dgButtonsPanel(canvas, bgView);
        //panel.setTranslateX(dgUtils.SCREENWIDTH / 5 );
        //panel.setTranslateY(dgUtils.SCREENHEIGHT - dgUtils.CONTROLHEIGHT);
        //gameWindow.getChildren().addAll(canvas, panel);
        gameWindow.getChildren().addAll(canvas);
        primaryStage.setTitle("超级连连看--麻俊鹏组");
        primaryStage.setResizable(false);

        // 添加图标
        Image iconImage = new Image("http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/icon.jpg");

        primaryStage.getIcons().add(dgUtils.iconImg);
    }

    public static void main(String[] args) {
        launch(args);
    }
    //}
}
