package org.csu.linkgame.service.ClassForDoubleGame;

import javafx.scene.image.Image;
import org.csu.linkgame.domain.GameLevel;
import org.csu.linkgame.mapper.GameLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class dgUtils {
    @Autowired
    private GameLevelMapper gameLevelMapper;
    // 游戏宽度
    public static int SCREENWIDTH = 1440;
    // 游戏高度
    public static int SCREENHEIGHT = 960;
    // 控制面板宽度
    public static int CONTROLWIDTH = 600;
    // 控制面板高度
    public static int CONTROLHEIGHT = 100;
    // 画布水平偏移
    public static int xOffset = 100;
    // 画布垂直偏移
    public static int yOffset = 50;
    // 单元格大小
    public static int ELEMENTSIZE = 60;
    // 单元格行数
    public static int ROWCOUNT = 12;
    // 单元格列数
    public static int COLCOUNT = 12;
    // 单元格图片
    public static String[] IMAGE_NAMES = { "unit1.jpg", "unit2.jpg",
            "unit3.jpg", "unit4.jpg", "unit5.jpg", "unit6.jpg",
            "unit7.jpg", "unit8.jpg", "unit9.jpg", "unit10.jpg",
            "unit11.jpg", "unit12.jpg"                   };
    // logo
    //public Image iconImg = new Image("file:D:\\软件工程实验\\实训\\java实训\\src\\main\\java\\org\\csu\\linkgame\\service\\ClassForGame\\images\\bg1.jpg");
    public static Image iconImg = new Image("http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/icon.jpg");
    // 背景图片
    static String bgImg = "http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/bg1.jpg";
    public static Image[] BG_IMAGE_NAMES = new Image[] {
            new Image(bgImg+"\\bg1.jpg"),
            new Image(bgImg+"\\bg2.jpg"),
            new Image(bgImg+"\\bg3.jpg"),
            new Image(bgImg+"\\bg4.jpg"),
            new Image(bgImg+"\\bg5.jpg"),
            new Image(bgImg+"\\bg6.jpg"),
    };
    public dgUtils(int level){
        init(level);
    }
    public void init(int level){
        List<GameLevel> gameLevels = gameLevelMapper.selectAllByLevel(level);
        if(gameLevels.size() == 0){
            return;
        }else{
            this.ELEMENTSIZE = gameLevels.get(0).getUnitSize();
            this.SCREENWIDTH = gameLevels.get(0).getSceneWidth();
            this.SCREENHEIGHT = gameLevels.get(0).getSceneLength();
            this.ROWCOUNT = gameLevels.get(0).getRowCount();
            this.COLCOUNT = gameLevels.get(0).getColCount();
        }
    }
}

