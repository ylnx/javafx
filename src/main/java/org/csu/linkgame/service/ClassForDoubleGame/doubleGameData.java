package org.csu.linkgame.service.ClassForDoubleGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 一个公用的数据类，供服务器调用
public class doubleGameData {
    // 存储剩余的Uint
    public static  Map<dgPoint, dgTile> dgMap;
    // 存储Uint的边界bound
    public static Map<dgPoint, dgBound> dgBoundMap;

    // 用户1的开始Unit
    public static dgTile user1Start;
    // 用户1的目标Unit
    public static dgTile user1End;

    // 用户2的开始Unit
    public static dgTile user2Start;
    // 用户2的目标Unit
    public static dgTile user2End;

    // 为了初始化数据
    public static List<Integer> randomRowList=new ArrayList<>();
    public static List<Integer> randomColList=new ArrayList<>();
    // 判断是否发送path信息
    public static int sendPathFlag = 0;
    // 要发送的path信息
    public static List<dgPoint> path = null;
    // 得分情况:
    public static int goals = 0;

}
