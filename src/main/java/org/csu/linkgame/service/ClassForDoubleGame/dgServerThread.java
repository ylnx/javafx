package org.csu.linkgame.service.ClassForDoubleGame;


import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class dgServerThread implements Runnable {

    public Socket socket;
    public int goals=0;
    public List<Integer> randomRowList;
    public List<Integer> randomColList;

    public dgServerThread (Socket socket,List<Integer> randomRowList,List<Integer> randomColList) throws IOException {
        this.socket = socket;
        this.randomRowList = randomRowList;
        this.randomColList = randomColList;
    }

    @Override
    public void run() {
        while (true) {
            try {
                ObjectInputStream is = null;
                is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
                Object obj = is.readObject();
                List<dgPoint> path = (List<dgPoint>) obj;
                for (Socket item : doubleGameServer.socketList) {
                    if (item != this.socket) {
                        ObjectOutputStream os = new ObjectOutputStream(item.getOutputStream());
                        os.writeObject(path);
                        System.out.println("这里是Server端:" + path);
                        os.flush();
                    } else {
                        System.out.println("item == this.socket");
                    }
                }
            /*BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String str = br.readLine();
                for (Socket item : doubleGameServer.socketList) {
                    PrintWriter pw = new PrintWriter(item.getOutputStream());
                    pw.println("用户"+socket.getPort()+"说："+str);
                    pw.flush();
                }
            }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
