package org.csu.linkgame.service.ClassForDoubleGame;


import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

/**
 * @author ylnx
 *
 */
public class dgButtonsPanel extends HBox {

/*	@FXML
	private AnchorPane anchorPane =new AnchorPane();*/

    private dgServiceImpl canvas;
    private ImageView bgView;

    /**
     * 控制面板实现
     */
    public dgButtonsPanel(dgServiceImpl canvas, ImageView bgView) {
        this.canvas = canvas;
        this.bgView = bgView;
        //开始按钮
        Button start = createButton("重新开始");
        start.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                org.csu.linkgame.service.ClassForDoubleGame.dgButtonsPanel.this.canvas.initGame();
/*				Random random = new Random();
				Image image = new Image("file:C:\\Users\\Lenovo\\Desktop\\image\\bg1.jpg");
				dgButtonsPanel.this.bgView.setImage(image
						//dgUtils.BG_IMAGE_NAMES[random.nextInt(dgUtils.BG_IMAGE_NAMES.length)]
						 );*/
                //dgClock.tmp = 0;
                //dgClock.flag = 1;
            }
        });

/*		//刷新按钮
		Button refresh = createButton("刷新");
		refresh.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				dgButtonsPanel.this.canvas.refresh();
			}
		});*/

        //提示按钮
        Button hit = createButton("提示");
        hit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                org.csu.linkgame.service.ClassForDoubleGame.dgButtonsPanel.this.canvas.search();
            }
        });
        //退出按钮
        Button exit = createButton("退出");
        setAlignment(Pos.CENTER);

        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Platform.exit();
                //dgClock.tmp = 0;
                //dgClock.flag = 0;

                // 获取当前窗口
                Stage window = (Stage) exit.getScene().getWindow();
                // 关闭窗口
                window.close();
            }
        });
        setPrefSize(dgUtils.CONTROLWIDTH, dgUtils.CONTROLHEIGHT);
//		setStyle(getClass().getResource("button.css").toExternalForm());
//		getStylesheets().add(getClass().getResource("button.css").toExternalForm());
        //setStyle("-fx-base: rgb(30,170,255);");
        //getChildren().addAll(start, refresh, hit, exit);
        getChildren().addAll(start, hit, exit);
/*		this.anchorPane.getChildren().addAll(start, hit, exit);
		this.anchorPane.getStylesheets().add("css/button.css");
		getChildren().add(anchorPane);*/
    }

    private Button createButton(String text) {
        Button button = new Button(text);
        button.setPrefSize(150, 50);
        BorderStroke bos = new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID,new CornerRadii(10),new BorderWidths(5));
        Border border = new Border(bos);
        button.setStyle("-fx-background-color: transparent;");
        button.setOnMousePressed(e->button.setStyle("-fx-background-color:LightGray"));
        button.setOnMouseEntered(e->onButtonHover(button));
        button.setOnMouseExited(e->button.setStyle("-fx-background-color:transparent"));
/*		try {
			URL url = new File("button.css").toURI().toURL();
			String encodedFileName = url.toString();
			System.out.println(encodedFileName);

			button.getStylesheets().add("D:\\软件工程实验\\实训\\java实训\\src\\main\\java\\org\\csu\\linkgame\\service\\ClassForGame\\css\\button.css");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}*/

        button.setBorder(border);
        //button.getStylesheets().add(getClass().getResource("css/button.css").toExternalForm());
        setMargin(button, new Insets(20));
        return button;
    }
    private void onButtonHover(Button button){
        button.setStyle("-fx-background-color:MistyRose");
    }

}
