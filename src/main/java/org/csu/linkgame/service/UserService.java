package org.csu.linkgame.service;

import org.apache.ibatis.annotations.Param;
import org.csu.linkgame.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
* @author Pluto
* @description 针对表【user】的数据库操作Service
* @createDate 2022-08-31 10:51:55
*/
public interface UserService extends IService<User> {
    // 封装注册方法
    public int userRegister(String username, String password);
    // 封装登录方法
    public int checkLogin(String username, String password);
    // 根据用户名返回User
    public User getUserInfoByUsername(String username);
    // 搜索全部用户信息
    public List<User> getAllUserInfo();
    // 根据用户名批量删除
    public int deleteUserByUsernameList(Collection<String> usernameList);

    public List<User> getUsersByKeyword(String keyword);

    public int insertNewUserInfo(User user);

    public int updateUserInfoCol(User user);
}
