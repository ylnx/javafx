package org.csu.linkgame.service;

import org.csu.linkgame.domain.GameLevel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
* @author Pluto
* @description 针对表【game_level】的数据库操作Service
* @createDate 2022-09-02 17:53:12
*/
public interface GameLevelService extends IService<GameLevel> {

    public List<GameLevel> getAllGameLevel();

    public int deleteGameLevelByLevelList(Collection<Integer> levelList);

    public List<GameLevel> getGameLevelsByKeyword(String keyword);

    public int insertNewGameLevel(GameLevel gameLevel);

    public int updateGameLevelCol(GameLevel gameLevel);
}
