package org.csu.linkgame.service.impl;

import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.Glow;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import org.csu.linkgame.LinkGameApplication;
import org.csu.linkgame.service.ClassForGame.*;
import org.csu.linkgame.service.GameService;

import java.util.*;

public class GameServiceImpl extends Group implements GameService {

    // 存储剩余的Uint
    public Map<Point, Tile> map = new HashMap<>();
    // 存储Uint对应的Bound
    public Map<Point, Bound> boundMap = new HashMap<>();

    // 开始Unit
    public Tile tempStart;
    // 目标Unit
    public Tile tempEnd;

    // 用于保存查找的可行解,也即可行路径
    private LinkedList<Point> resultPath = new LinkedList<>();
    // LinkedList是一个类似于ArrayList的列表，但它在充当队列queue和栈stack的时候有很好的效果
    // 不过这里主要是看网上的参考代码时，网上用的就是LinkedList，所以就参考着写了


    public GameServiceImpl() {
        initGame();
    }
    // 动态刷新界面
    public void refresh() {
        List<KeyValue> values = new LinkedList<>();

        // 随机交互两个Unit图标的位置
        for (Point point : map.keySet()) {
            Tile unit = map.get(point);
            int randomNum = new Random().nextInt(map.size());
            Tile randomUnit = (Tile) this.getChildren().get(randomNum);
            Point randomTempIndex = randomUnit.tempPoint;
            randomUnit.tempPoint = unit.tempPoint;
            unit.tempPoint = randomTempIndex;
        }
        for (Point point : map.keySet()) {
            Tile unit = map.get(point);
            KeyValue valueX = new KeyValue(unit.translateXProperty(),
                    unit.tempPoint.col * (Utils.ELEMENTSIZE + 5) + 60,
                    Interpolator.EASE_IN);
            KeyValue valueY = new KeyValue(unit.translateYProperty(),
                    unit.tempPoint.row * (Utils.ELEMENTSIZE + 5) + 60,
                    Interpolator.EASE_IN);
            values.add(valueX);
            values.add(valueY);
        }
        // 动画实现交互过程
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);

        KeyFrame frame = new KeyFrame(Duration.seconds(1.5),
                values.toArray(new KeyValue[0]));
        timeline.getKeyFrames().add(frame);
        timeline.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (Node node : getChildren()) {
                    if (node instanceof Tile) {
                        Tile unit = (Tile) node;
                        unit.index.set(unit.tempPoint);
                        map.put(unit.getPoint(), unit);
                    }
                }
            }
        });
        timeline.play();
    }

    //初始化游戏
    public void initGame() {
        getChildren().clear();
        map.clear();
        boundMap.clear();
        tempStart = null;
        tempEnd = null;
        create();
    }

    // 提示功能
    public void search() {
        for (Point startPoint : map.keySet()) {
            tempStart = map.get(startPoint);
            // 遍历存在的Uint
            for (Point endPoint : map.keySet()) {
                tempEnd = map.get(endPoint);
                if (tempStart != tempEnd
                        && tempStart.getType().equals(tempEnd.getType())) {
                    List<Point> list = findPath(startPoint, endPoint);
                    if (list != null) {
                        // 高亮显示查找结果
                        Glow glow = new Glow(0);
                        tempStart.setEffect(glow);
                        tempEnd.setEffect(glow);
                        tempStart = null;
                        tempEnd = null;
                        Timeline time = new Timeline();
                        KeyValue value = new KeyValue(glow.levelProperty(), 1);
                        KeyFrame frame = new KeyFrame(Duration.seconds(1),
                                value);
                        time.getKeyFrames().add(frame);
                        time.setAutoReverse(true);
                        time.setCycleCount(10);
                        time.play();
                        return;
                    }
                }
            }
        }
    }

    public void create() {
        setTranslateX(Utils.xOffset);
        setTranslateY(Utils.yOffset);
        for (int row = -1; row <= Utils.ROWCOUNT; row++) {
            for (int col = -1; col <= Utils.COLCOUNT; col++) {
                // 设置最外层移动点(注意最外层没有Unit图标，只有移动点，用于实现消除单元格是的动画效果)
                if (row == -1 || row == Utils.ROWCOUNT || col == -1
                        || col == Utils.COLCOUNT) {
                    double startX = col * (Utils.ELEMENTSIZE + 5) + 60;
                    double startY = row * (Utils.ELEMENTSIZE + 5) + 60;
                    Bound bound = new Bound(startX, startY, startX
                            + Utils.ELEMENTSIZE, startY + Utils.ELEMENTSIZE);
                    Point point = new Point(row, col);
                    boundMap.put(point, bound);
                }// 其它点都有对应的Unit图标
                else {
                    Point point = new Point(row, col);
                    Tile unit = new Tile(this, point, Utils.IMAGE_NAMES[(row
                            * Utils.COLCOUNT + col)
                            //% Utils.IMAGE_NAMES.length]);
                            % Utils.COLCOUNT]);
                    getChildren().add(unit);
                    Bounds bounds = unit.getBoundsInParent();
                    Bound bound = new Bound(bounds.getMinX(), bounds.getMinY(),
                            bounds.getMaxX(), bounds.getMaxY());
                    boundMap.put(point, bound);
                    map.put(unit.getPoint(), unit);
                }
            }
        }
        // 随机打乱图标
        Random random = new Random();
        for (int row = 0; row < Utils.ROWCOUNT; row++) {
            for (int col = 0; col < Utils.COLCOUNT; col++) {
                int randomRow = random.nextInt(Utils.ROWCOUNT);
                int randomCol = random.nextInt(Utils.COLCOUNT);
                Tile unit = (Tile) getChildren()
                        .get(row * Utils.COLCOUNT + col);
                Tile randomUnit = (Tile) getChildren().get(
                        randomRow * Utils.COLCOUNT + randomCol);
                Point randomIndex = randomUnit.getPoint();
                randomUnit.index.set(unit.getPoint());
                unit.index.set(randomIndex);
                map.put(randomUnit.getPoint(), randomUnit);
                map.put(unit.getPoint(), unit);
            }
        }
    }
    // 查找可行解
    public List<Point> findPath(Point start, Point end) {
        // 此处的start和end是指用户选取的第一个点与第二个点

        // count是为了后续判断应该向何处遍历
        start.count = -1;
        end.count = -1;
        // 清除可行解的列表
        resultPath.clear();
        // 将开始位置加入resultPath
        resultPath.add(start);
        Point curPoint = null;
        Point neighbor = null;

        // 存储所有可行的连接路径，方便后面选择最优解
        List<LinkedList<Point>> resultList = new ArrayList<>();

        // 当resultPath中节点不为空的时候，进行一个dfs的遍历，得到所有可行解
        while (!resultPath.isEmpty()) {
            curPoint = resultPath.getLast();
            neighbor = getNextNeighbor(curPoint);
            // 有邻居节点时继续查找
            if (neighbor != null) {
                // 邻居节点为目标节点，说明路径查找成功
                if (neighbor.equals(end)) {
                    resultPath.addLast(neighbor);

                    // resultList中存储所有的可行解
                    resultList.add(new LinkedList<>(resultPath));

                    resultPath.removeLast();
                } else {
                    resultPath.addLast(neighbor);
                    boolean flag = checkCornerLessthan2(resultPath);
                    // 判断加入节点后，连接路径转弯是否大于2，如果大于2说明路径不符合要求
                    // 因为连连看的游戏规则要求不能有超过两个折线
                    if (!flag) {
                        resultPath.removeLast();
                    }
                }
            }
            // 没有邻居节点时，将该节点从resultPath中删除，继续遍历上一个节点
            else {
                resultPath.removeLast();
            }
        }
        return chooseResult(resultList);
    }

    // 选择最优连接路径
    public List<Point> chooseResult(List<LinkedList<Point>> resultList) {
        if (resultList.isEmpty()) {
            return null;
        }
        // 根据节点数排序，因为每条路径里的节点都是相邻节点，因此节点数就代表路径长度
        Collections.sort(resultList, new Comparator<List<Point>>() {
            @Override
            public int compare(List<Point> o1, List<Point> o2) {
                return o1.size() - o2.size();
            }
        });
        for (List<Point> path : resultList) {
            boolean flag = checkCornerLessthan2(path);
            if (flag) {
                return path;
            }
        }

        return null;
    }

    public Point getNextNeighbor(Point point) {
        // 为了不重复获取邻居节点，每获取一次邻居节点，获取次数加1，
        point.count++;
        int row = point.row;
        int col = point.col;
        Tile neighbor = null;
        // 默认从左开始进行查找
        if (point.count == 0) {
            Point left = new Point(row, col - 1);
            neighbor = map.get(left);
            //邻居是目标位置,返回该位置
            if (neighbor == tempEnd) {
                return left;
            }
            //邻居位置为空,且不在包含的路径中(避免回路),返回该位置
            if (neighbor == null && (col - 1) >= -1
                    && !resultPath.contains(left)) {
                return left;
            } else {
                neighbor = null;
                point.count++;
            }
        }
        //count=1时向右寻路
        if (point.count == 1) {
            Point right = new Point(row, col + 1);
            neighbor = map.get(right);
            if (neighbor == tempEnd) {
                return right;
            }
            if (neighbor == null && (col + 1) <= Utils.COLCOUNT
                    && !resultPath.contains(right)) {
                return right;
            } else {
                neighbor = null;
                point.count++;
            }
        }
        //count=2时向上寻路
        if (point.count == 2) {
            Point up = new Point(row - 1, col);
            neighbor = map.get(up);
            if (neighbor == tempEnd) {
                return up;
            }
            if (neighbor == null && (row - 1) >= -1 && !resultPath.contains(up)) {
                return up;
            } else {
                neighbor = null;
                point.count++;
            }
        }
        //count=3时向下寻路
        if (point.count == 3) {
            Point down = new Point(row + 1, col);
            neighbor = map.get(down);
            if (neighbor == tempEnd) {
                return down;
            }
            if (neighbor == null && (row + 1) <= Utils.ROWCOUNT
                    && !resultPath.contains(down)) {
                return down;
            } else {
                neighbor = null;
                point.count++;
            }
        }

        return null;
    }
    public boolean checkCornerLessthan2(List<Point> path) {
        //小于4个点，拐弯肯定<=2
        if (path.size() <= 4) {
            return true;
        } else {
            int cross = 0;
            for (int i = 0; i < path.size() - 2; i++) {
                Point first = path.get(i);
                Point second = path.get(i + 1);
                Point third = path.get(i + 2);
                int dx1 = second.col - first.col;
                int dx2 = third.col - second.col;
                int dy1 = second.row - first.row;
                int dy2 = third.row - second.row;
                //判断连续3个点是否在一条直线上
                if (dx1 != dx2 || dy1 != dy2) {
                    cross++;
                }
            }
            if (cross <= 2) {
                return true;
            } else {
                return false;
            }
        }
    }
    public void drawLine(final List<Point> path) {
        final Tile start = map.get(path.get(0));
        final Tile end = map.get(path.get(path.size() - 1));
        Path track = new Path();
        //将查找到中的点加入Path对象中
        for (int i = 0; i < path.size(); i++) {
            Bound bound = boundMap.get(path.get(i));
            double centerX = (bound.getStartX() + bound.getEndX()) / 2;
            double centerY = (bound.getStartY() + bound.getEndY()) / 2;
            if (i == 0) {
                track.getElements().add(new MoveTo(centerX, centerY));
            } else {
                track.getElements().add(new LineTo(centerX, centerY));
            }
        }
        Duration duration = Duration.seconds(0.1 * path.size());

        // 位置变换特效
        PathTransition pathTransition = PathTransitionBuilder.create()
                .node(start).orientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT)
                .path(track).duration(duration).build();
        pathTransition.setOnFinished(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                getChildren().remove(start);
                getChildren().remove(end);
                map.remove(start.getPoint());
                map.remove(end.getPoint());
                if (getChildren().isEmpty()) {
                    Clock.flag=0;
                    Text text = new Text("恭喜您通关!\n您所用时长为:"+Clock.tmp+"s");

                    final WebView browser = new WebView();
                    final WebEngine webEngine = browser.getEngine();
                    // TODO check
                    String url = LinkGameApplication.class.getResource("view/index.html").toExternalForm();
                    webEngine.load(url);
                    ScrollPane scrollPane = new ScrollPane();
                    scrollPane.setContent(browser);
                    getChildren().add(scrollPane);

                    text.setTranslateX(Utils.SCREENWIDTH / 2 -200);
                    System.out.println(Utils.SCREENWIDTH);
                            //- Utils.CONTROLWIDTH / 2);
                    text.setTranslateY(Utils.SCREENHEIGHT / 2-100);
                    text.setFont(javafx.scene.text.Font.font(32));
                    getChildren().add(text);
                }
            }
        });
        // 实现选择特效
        RotateTransition rotateTransition = RotateTransitionBuilder.create()
                .duration(duration).byAngle(360 * path.size()).build();
        // 并行执行
        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.setNode(start);
        parallelTransition.getChildren().addAll(pathTransition,
                rotateTransition);
        parallelTransition.play();
    }
}
