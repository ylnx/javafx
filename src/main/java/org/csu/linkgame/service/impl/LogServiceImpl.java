package org.csu.linkgame.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.csu.linkgame.domain.Log;
import org.csu.linkgame.service.LogService;
import org.csu.linkgame.mapper.LogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
* @author Pluto
* @description 针对表【log】的数据库操作Service实现
* @createDate 2022-09-02 16:48:17
*/
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log>
    implements LogService{

    @Autowired
    private LogMapper logMapper;

    public List<Log> getAllLog() {
        return logMapper.selectAll();
    }

    public int deleteLogByLogIdList(Collection<Integer> logIdList) {
        return logMapper.deleteByLogIdIn(logIdList);
    }

    public List<Log> getLogsByKeyword(String keyword) {
        return logMapper.selectByKeyword(keyword);
    }

    public int insertLog(String log) {
        Log log1 = new Log();
        log1.setLogInfo(log);
        log1.setLogTime(new Date());
        return logMapper.insertAll(log1);
    }
}




