package org.csu.linkgame.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.csu.linkgame.domain.GameLevel;
import org.csu.linkgame.service.GameLevelService;
import org.csu.linkgame.mapper.GameLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
* @author Pluto
* @description 针对表【game_level】的数据库操作Service实现
* @createDate 2022-09-02 17:53:12
*/
@Service
public class GameLevelServiceImpl extends ServiceImpl<GameLevelMapper, GameLevel>
    implements GameLevelService{

    @Autowired
    private GameLevelMapper gameLevelMapper;

    public List<GameLevel> getAllGameLevel() {
        return gameLevelMapper.selectAll();
    }

    public int deleteGameLevelByLevelList(Collection<Integer> levelList) {
        return gameLevelMapper.deleteByLevelIn(levelList);
    }

    public List<GameLevel> getGameLevelsByKeyword(String keyword) {
        return gameLevelMapper.selectByKeyword(keyword);
    }

    public int insertNewGameLevel(GameLevel gameLevel) {
        return gameLevelMapper.insertAll(gameLevel);
    }

    public int updateGameLevelCol(GameLevel gameLevel) {
        return gameLevelMapper.updateSelective(gameLevel);
    }
}




