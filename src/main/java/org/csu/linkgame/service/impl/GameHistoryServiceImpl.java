package org.csu.linkgame.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.csu.linkgame.domain.GameHistory;
import org.csu.linkgame.service.GameHistoryService;
import org.csu.linkgame.mapper.GameHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
* @author Pluto
* @description 针对表【game_history】的数据库操作Service实现
* @createDate 2022-09-02 16:43:22
*/
@Service
public class GameHistoryServiceImpl extends ServiceImpl<GameHistoryMapper, GameHistory>
    implements GameHistoryService{

    @Autowired
    private GameHistoryMapper gameHistoryMapper;

    public List<GameHistory> getAllGameHistory() {
        return gameHistoryMapper.selectALl();
    }

    public int deleteGameHistoryByGameHistoryIdList(Collection<Integer> gameHistoryIdList) {
        return gameHistoryMapper.deleteByGameHistoryIdIn(gameHistoryIdList);
    }

    public List<GameHistory> getGameHistoriesByKeyword(String keyword) {
        return gameHistoryMapper.selectByKeyword(keyword);
    }
}




