package org.csu.linkgame.service;

import org.csu.linkgame.service.ClassForGame.Point;

import java.util.LinkedList;
import java.util.List;

public interface GameService {
    public List<Point> findPath(Point start, Point end);
    public List<Point> chooseResult(List<LinkedList<Point>> resultList);
    public Point getNextNeighbor(Point point);
    public boolean checkCornerLessthan2(List<Point> path);
}
