package org.csu.linkgame.service;

import org.csu.linkgame.domain.Log;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
* @author Pluto
* @description 针对表【log】的数据库操作Service
* @createDate 2022-09-02 16:48:17
*/
public interface LogService extends IService<Log> {

    public List<Log> getAllLog();

    public int deleteLogByLogIdList(Collection<Integer> logIdList);

    public List<Log> getLogsByKeyword(String keyword);

    public int insertLog(String log);
}
