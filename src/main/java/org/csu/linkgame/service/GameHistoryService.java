package org.csu.linkgame.service;

import org.csu.linkgame.domain.GameHistory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
* @author Pluto
* @description 针对表【game_history】的数据库操作Service
* @createDate 2022-09-02 16:43:22
*/
public interface GameHistoryService extends IService<GameHistory> {

    public List<GameHistory> getAllGameHistory();

    public int deleteGameHistoryByGameHistoryIdList(Collection<Integer> gameHistoryIdList);

    public List<GameHistory> getGameHistoriesByKeyword(String keyword);
}
