package org.csu.linkgame.service.ClassForGame;

/**
 * @author hanxi
 * 
 */
public class Bound {
	//开始x位置
	private double startX;
	//开始y位置
	private double startY;
	//结束x位置
	private double endX;
	//结束y位置
	private double endY;

	public Bound(double startX, double startY, double endX, double endY) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
	}

	public double getStartX() {
		return startX;
	}

	public double getStartY() {
		return startY;
	}

	public double getEndX() {
		return endX;
	}

	public double getEndY() {
		return endY;
	}

	public void setStartX(double startX) {
		this.startX = startX;
	}

	public void setStartY(double startY) {
		this.startY = startY;
	}

	public void setEndX(double endX) {
		this.endX = endX;
	}

	public void setEndY(double endY) {
		this.endY = endY;
	}

}
