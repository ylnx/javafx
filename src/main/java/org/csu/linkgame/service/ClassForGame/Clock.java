package org.csu.linkgame.service.ClassForGame;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
public class Clock extends Pane {

    public static Timeline animation = new Timeline();
    private String S = "";
    public static int tmp = 0;

    public static int flag = 1;

    Label label = new Label("0");


    public Clock() {
        label.setFont(javafx.scene.text.Font.font(20));

        getChildren().add(label);
        animation = new Timeline(new KeyFrame(Duration.millis(1000), e -> timelabel()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
    }

    public void timelabel() {
        if(flag==1){
            tmp++;
            S = "当前用时为:"+tmp + "s";
            label.setText(S);

        }

/*        if(tmp==0){
            animation.stop();
        }*/
    }

}
