package org.csu.linkgame.service.ClassForGame;


/**
 * @author ylnx
 *
 */
public class Point {
    // 所在行
    public int row;
    // 所在列
    public int col;
    // 用于查询邻居节点时记录访问次数
    public int count = -1;

    public Point(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        Point p = (Point) obj;
        return (this.row == p.row) && (this.col == p.col);
    }

    @Override
    public String toString() {
        return "[" + row + "," + col + "," + count + "]";
    }
}
