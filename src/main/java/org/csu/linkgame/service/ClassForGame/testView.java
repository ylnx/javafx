package org.csu.linkgame.service.ClassForGame;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.csu.linkgame.service.impl.GameServiceImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Random;

public class testView extends Application {
/*    @Autowired
    private GamesettingService gamesettingService;*/
// MySQL 8.0 以上版本 - JDBC 驱动名及数据库 URL
static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/super_link?useUnicode=true&characterEncoding=utf8";


    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "rootatcsu";

    public int level = 0;
    public AnchorPane gameWindow;
    public testView(int level,AnchorPane gameWindow){
        this.level = level;
        this.gameWindow = gameWindow;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // 监听窗口关闭
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Clock.tmp = 0;
                Clock.flag = 0;

                // 关闭这条时间线
                Clock.animation.stop();
            }
        });
        Clock clock = new Clock();

        Connection conn = null;
        Statement stmt = null;

        // 注册 JDBC 驱动
        Class.forName(JDBC_DRIVER);

        // 打开链接
        //System.out.println("连接数据库...");
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        //System.out.println(" 实例化Statement对象...");
        stmt = conn.createStatement();
        String sql;
        sql = "select * from game_level\n" +
                "where level = "+level;
        ResultSet rs = stmt.executeQuery(sql);

        // 展开结果集数据库
        while(rs.next()){
            // 通过字段检索
            Utils.ELEMENTSIZE = rs.getInt("unit_size");
            Utils.SCREENWIDTH = rs.getInt("scene_width");
            Utils.SCREENHEIGHT = rs.getInt("scene_length");
            Utils.ROWCOUNT = rs.getInt("row_count");
            Utils.COLCOUNT = rs.getInt("col_count");
        }
        // 完成后关闭
        rs.close();
        stmt.close();
        conn.close();
        //System.out.println("Goodbye!");

/*        System.out.println(gamesettingService);
        Gamesetting gameSetting = gamesettingService.getGamesettingInfoByLevel(0);

        Utils.ELEMENTSIZE = gameSetting.getUnitSize();
        Utils.SCREENWIDTH = gameSetting.getSceenWidth();
        Utils.SCREENHEIGHT = gameSetting.getSceenLength();
        Utils.ROWCOUNT = gameSetting.getRowCount();
        Utils.COLCOUNT = gameSetting.getColCount();*/

        // 随机生成背景图片
        Random random = new Random();

        //Image image = new Image("file:images/bg1.jpg");
        //Image image = new Image("file:D:\\软件工程实验\\实训\\java实训\\src\\main\\java\\org\\csu\\linkgame\\service\\ClassForGame\\images\\loginBackImg2.png");
        Image image = new Image("http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/bg1.jpg");
        /*ImageView bgView = new ImageView(
                Utils.BG_IMAGE_NAMES[random
                        .nextInt(Utils.BG_IMAGE_NAMES.length)]);*/
        ImageView bgView = new ImageView();
        bgView.setImage(image);
        bgView.setFitHeight(Utils.SCREENHEIGHT);
        bgView.setFitWidth(Utils.SCREENWIDTH);
        //bgSound.play();
        gameWindow.getChildren().add(bgView);
/*        if(Utils.ROWCOUNT>10){
            bgView.setFitHeight(Utils.SCREENWIDTH*(0.1+Utils.ROWCOUNT/10));
            bgView.setFitWidth(Utils.SCREENHEIGHT*(0.1+Utils.ROWCOUNT/10));
            //bgSound.play();
            root.getChildren().add(bgView);
        }else{
            bgView.setFitHeight(Utils.SCREENHEIGHT);
            bgView.setFitWidth(Utils.SCREENWIDTH);
            //bgSound.play();
            root.getChildren().add(bgView);
        }*/
        GameServiceImpl canvas = new GameServiceImpl();
        ButtonsPanel panel = new ButtonsPanel(canvas, bgView);
        //panel.setTranslateX(Utils.SCREENWIDTH - Utils.CONTROLWIDTH);
        panel.setTranslateX(Utils.SCREENWIDTH / 5 );
        panel.setTranslateY(Utils.SCREENHEIGHT - Utils.CONTROLHEIGHT);
        gameWindow.getChildren().addAll(canvas, panel);
        gameWindow.getChildren().add(clock);
        //Scene scene = new Scene(root, Utils.SCREENWIDTH, Utils.SCREENHEIGHT);
        /*if(Utils.ROWCOUNT>10){
*//*            bgView.setFitHeight(Utils.SCREENWIDTH*(0.1+Utils.ROWCOUNT/10));
            bgView.setFitWidth(Utils.SCREENHEIGHT*(0.1+Utils.ROWCOUNT/10));
            //bgSound.play();
            root.getChildren().add(bgView);*//*
            scene = new Scene(root, Utils.SCREENWIDTH*(0.1+Utils.ROWCOUNT/10)
                    , Utils.SCREENHEIGHT*(0.1+Utils.ROWCOUNT/10));
        } else{
*//*            bgView.setFitHeight(Utils.SCREENHEIGHT);
            bgView.setFitWidth(Utils.SCREENWIDTH);
            //bgSound.play();
            root.getChildren().add(bgView);*//*
            scene = new Scene(root, Utils.SCREENWIDTH, Utils.SCREENHEIGHT);
        }*/
/*        scene.setFill(Color.GRAY);
//        scene.getStylesheets().add(getClass().getResource("css/button.css").toExternalForm());
        primaryStage.setScene(scene);*/
        primaryStage.setTitle("超级连连看--麻俊鹏组");
        primaryStage.setResizable(false);

        // 添加图标
        Image iconImage = new Image("http://orange-1312206514.cos.ap-guangzhou.myqcloud.com/images/icon.jpg");
        //ImageView iconView = new ImageView();
        //iconView.setImage(iconImage);
        //System.out.println(Utils.iconImg);
        primaryStage.getIcons().add(Utils.iconImg);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    //}
}
