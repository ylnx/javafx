package org.csu.linkgame.service;

public interface OSSService {
    String upload(String localFilePath, String keyPath);
}
