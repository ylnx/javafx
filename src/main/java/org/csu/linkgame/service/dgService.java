package org.csu.linkgame.service;



import org.csu.linkgame.service.ClassForDoubleGame.dgPoint;

import java.util.LinkedList;
import java.util.List;

public interface dgService {
    public List<dgPoint> findPath(dgPoint start, dgPoint end);
    public List<dgPoint> chooseResult(List<LinkedList<dgPoint>> resultList);
    public dgPoint getNextNeighbor(dgPoint point);
    public boolean checkCornerLessthan2(List<dgPoint> path);
}
