package org.csu.linkgame;

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import de.felixroske.jfxsupport.SplashScreen;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.csu.linkgame.view.*;
import org.mybatis.spring.annotation.MapperScan;
import org.pan.decorate.StageDecorate;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "org.csu.linkgame.mapper")
public class LinkGameApplication extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(LinkGameApplication.class.getResource("./view/login.fxml"));
        StageDecorate.decorate(primaryStage,root);
        primaryStage.setTitle("超级连连看٩( 'ω' )و ");
        primaryStage.getIcons().add(new Image("/org/csu/linkgame/images/icon.png"));
        primaryStage.show();
    }

}
