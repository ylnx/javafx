package org.csu.linkgame.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.csu.linkgame.service.ClassForGame.Clock;
import org.csu.linkgame.service.ClassForGame.testView;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import static javafx.application.Application.launch;

@FXMLController
public class SoloViewController implements Initializable  {
    @FXML
    public Button chooseModelButton;
    @FXML
    public Button promptButton;

    @FXML
    public AnchorPane gameWindow;
/*    extends Application
    @FXML
    private Label remindLabel;
    @FXML
    private Button sampleButton;*/
    @Override
    public void initialize(URL location, ResourceBundle resources) {
/*        // 获取当前窗口
        Stage window = (Stage) sampleButton.getScene().getWindow();
        try {
            this.start(window);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }*/
    }
    public void onChooseModelButtonClicked(MouseEvent mouseEvent) throws Exception {
// 选择难度
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("难度选择");
        alert.setHeaderText("请在下方按钮选择你要进行的游戏难度");
        alert.setContentText("分为高级、中级、初级");

        ButtonType buttonTypeOne = new ButtonType("高级");
        ButtonType buttonTypeTwo = new ButtonType("中级");
        ButtonType buttonTypeThree = new ButtonType("初级");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree, buttonTypeCancel);

        int level = 0;
        Optional<ButtonType> result = alert.showAndWait();
        // 获取当前窗口
        Stage window = (Stage) chooseModelButton.getScene().getWindow();
        if (result.get() == buttonTypeOne){
            Clock.tmp=0;
            Clock.flag=0;
            Clock.animation.stop();
            // ... user chose "One"
            level = 2;
            Clock.flag = 1;
            gameWindow.getChildren().clear();
            // 打开单人模式界面
            testView open  = new testView(level,gameWindow);
            open.start(window);
        } else if (result.get() == buttonTypeTwo) {
            Clock.tmp=0;
            Clock.flag=0;
            Clock.animation.stop();
            // ... user chose "Two"
            level = 1;
            Clock.flag = 1;

            // 打开单人模式界面
            gameWindow.getChildren().clear();
            testView open  = new testView(level,gameWindow);
            open.start(window);
        } else if (result.get() == buttonTypeThree) {
            Clock.tmp=0;
            Clock.flag=0;
            Clock.animation.stop();
            // ... user chose "Three"
            level = 0;
            Clock.flag = 1;

            // 打开单人模式界面
            gameWindow.getChildren().clear();
            testView open  = new testView(level,gameWindow);
            open.start(window);
        } else {
            Clock.tmp=0;
            Clock.flag=0;
            Clock.animation.stop();
            //gameWindow.getChildren().clear();
            System.out.println("关闭窗口");
        }
    }



    /**
     * @author ylnx
     *
     */
    //public class Main extends Application {
        // 背景音乐(魔兽进行曲)
    /*AudioClip bgSound = AudioClipBuilder.create()
            .source(getClass().getResource("audios/war3.mp3").toExternalForm())
            .cycleCount(Integer.MAX_VALUE).build();*/



}
