package org.csu.linkgame.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.csu.linkgame.domain.User;
import org.csu.linkgame.service.ClassForDoubleGame.dgUtils;
import org.csu.linkgame.service.ClassForDoubleGame.doubleGameData;
import org.csu.linkgame.service.ClassForDoubleGame.doubleGameServer;
import org.csu.linkgame.service.ClassForDoubleGame.userGameView;
import org.csu.linkgame.service.ClassForGame.testView;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

@FXMLController
public class DoubleViewController implements Initializable {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/super_link?useUnicode=true&characterEncoding=utf8";


    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "rootatcsu";
    @FXML
    public Button startButton;
    @FXML
    public Button prepareButton;
    @FXML
    public Button cancelPrepareButton;
    @FXML
    public AnchorPane gameWindow;
    User user;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void onStartButtonClicked(MouseEvent mouseEvent) throws Exception {
        // 获取当前窗口
        Stage window = (Stage) prepareButton.getScene().getWindow();
        // 监听窗口关闭
        window.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    Connection conn = null;
                    Statement stmt = null;
                    Class.forName(JDBC_DRIVER);
                    conn = DriverManager.getConnection(DB_URL,USER,PASS);
                    stmt = conn.createStatement();
                    String sql;
                    sql = "update doublegame set userNumber = userNumber-1";
                    stmt.executeUpdate(sql);
                    // 完成后关闭
                    conn.close();
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();;
                }
                prepareButton.setDisable(false);
                cancelPrepareButton.setDisable(true);
            }
        });
        try {
            Connection conn = null;
            Statement stmt = null;
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            stmt = conn.createStatement();
            String sql;
            sql = "select * from doublegame";
            ResultSet rs = stmt.executeQuery(sql);
            int userNumber=0;
            while(rs.next()){
                userNumber = rs.getInt("userNumber");
            }
            // 完成后关闭
            rs.close();
            conn.close();
            stmt.close();
            if(userNumber==2){
                gameWindow.getChildren().clear();
                // 打开双人模式界面
                userGameView ugView = new userGameView(doubleGameData.dgMap,doubleGameData.dgBoundMap,gameWindow);
                ugView.start(new Stage());
            }else{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("当前准备人数为："+userNumber+",未达两人！");
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
/*        gameWindow.getChildren().clear();
        // 打开双人模式界面
        userGameView ugView = new userGameView(doubleGameData.dgMap,doubleGameData.dgBoundMap,gameWindow);
        ugView.start(new Stage());*/
    }

    public void onPrepareButtonClicked(MouseEvent mouseEvent) throws Exception {
        // 获取当前窗口
        Stage window = (Stage) prepareButton.getScene().getWindow();
        // 监听窗口关闭
        window.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    Connection conn = null;
                    Statement stmt = null;
                    Class.forName(JDBC_DRIVER);
                    conn = DriverManager.getConnection(DB_URL,USER,PASS);
                    stmt = conn.createStatement();
                    String sql;
                    sql = "update doublegame set userNumber = userNumber-1";
                    stmt.executeUpdate(sql);
                    // 完成后关闭
                    conn.close();
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();;
                }
                prepareButton.setDisable(false);
                cancelPrepareButton.setDisable(true);
            }
        });
        Connection conn = null;
        Statement stmt = null;
        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        stmt = conn.createStatement();
        String sql;
        sql = "update doublegame set userNumber = userNumber+1";
        stmt.executeUpdate(sql);
        // 完成后关闭
        conn.close();
        stmt.close();
        prepareButton.setDisable(true);
        cancelPrepareButton.setDisable(false);
    }
    public void onCancelPrepareButtonClicked(MouseEvent mouseEvent) throws Exception {
        // 获取当前窗口
        Stage window = (Stage) prepareButton.getScene().getWindow();
        // 监听窗口关闭
        window.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    Connection conn = null;
                    Statement stmt = null;
                    Class.forName(JDBC_DRIVER);
                    conn = DriverManager.getConnection(DB_URL,USER,PASS);
                    stmt = conn.createStatement();
                    String sql;
                    sql = "update doublegame set userNumber = userNumber-1";
                    stmt.executeUpdate(sql);
                    // 完成后关闭
                    conn.close();
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();;
                }
                prepareButton.setDisable(false);
                cancelPrepareButton.setDisable(true);
            }

        });
        Connection conn = null;
        Statement stmt = null;
        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        stmt = conn.createStatement();
        String sql;
        sql = "update doublegame set userNumber = userNumber-1";
        int rs = stmt.executeUpdate(sql);
        // 完成后关闭
        conn.close();
        stmt.close();
        cancelPrepareButton.setDisable(true);
        prepareButton.setDisable(false);
    }

}
