package org.csu.linkgame.controller;

import com.leewyatt.rxcontrols.controls.RXTextField;
import com.leewyatt.rxcontrols.controls.RXTranslationButton;
import de.felixroske.jfxsupport.FXMLController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.csu.linkgame.utils.AlertView;

import java.net.URL;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;

@FXMLController
public class LevelViewController implements Initializable {
    @FXML
    public RXTextField levelTextField;
    @FXML
    public RXTextField unitSizeTextField;
    @FXML
    public RXTextField sceneLengthTextField;
    @FXML
    public RXTextField sceneWidthTextField;
    @FXML
    public RXTextField colCountTextField;
    @FXML
    public RXTextField rowCountTextField;
    @FXML
    public RXTranslationButton submitButton;
    @FXML
    public RXTranslationButton cancelButton;

    AlertView alertView;

    private HashMap<String, Object> newGameLevelHashMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public HashMap getNewGameLevelHashMap() {
        return this.newGameLevelHashMap;
    }
    public void setNewGameLevelHashMap(HashMap newGameLevelHashMap) {
        this.newGameLevelHashMap = newGameLevelHashMap;
    }


    public void submitGameLevel(ActionEvent event) {
        if (!levelTextField.getText().equals("") && !unitSizeTextField.getText().equals("") && !sceneWidthTextField.getText().equals("") &&
                !sceneLengthTextField.getText().equals("") &&  !rowCountTextField.getText().equals("") && !colCountTextField.getText().equals("")) {
            alertView = new AlertView(Alert.AlertType.CONFIRMATION);
            alertView.setTitle("管理员请确认！");
            alertView.setHeaderText("你提交新的用户信息！");
            alertView.setContentText("是否提交？请选择！！！");
            Optional<ButtonType> result = alertView.showAndWait();
            if (result.get() == ButtonType.OK) {
                newGameLevelHashMap.put("level", levelTextField.getText());
                newGameLevelHashMap.put("unitSize", unitSizeTextField.getText());
                newGameLevelHashMap.put("sceneWidth", sceneWidthTextField.getText());
                newGameLevelHashMap.put("sceneLength", sceneLengthTextField.getText());
                newGameLevelHashMap.put("rowCount", rowCountTextField.getText());
                newGameLevelHashMap.put("colCount", colCountTextField.getText());
                newGameLevelHashMap.put("isChange", true);
                Stage stage = (Stage) submitButton.getScene().getWindow();
                stage.close();
            } else {
                event.consume();
            }
        } else {
            alertView = new AlertView(Alert.AlertType.WARNING);
            alertView.setTitle("管理员请确认！");
            alertView.setHeaderText("信息填写不完整！");
            alertView.setContentText("请仔细检查！！！");
            alertView.showAndWait();
        }

    }

    public void cancel(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

}
